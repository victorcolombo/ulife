/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.view;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.hackathon.ulife.R;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by victorcolombo on 5/16/15.
 */
public class CreateCourseActivity extends Activity {

    private Spinner spinnerWeekDay;
    private Spinner spinnerStartTime;
    private Spinner spinnerEndTime;
    private Spinner spinnerWeekDay2;
    private Spinner spinnerStartTime2;
    private Spinner spinnerEndTime2;
    private EditText editCourseName;
    private Button buttonCreate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.createcourse);

        buttonCreate = (Button) findViewById(R.id.buttonCreate);

        editCourseName = (EditText) findViewById(R.id.editCourseName);

        spinnerWeekDay = (Spinner) findViewById(R.id.spinnerWeekDay);
        spinnerWeekDay.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.weekday_array)));
        spinnerStartTime = (Spinner) findViewById(R.id.spinnerStartTime);
        spinnerStartTime.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.times_array)));
        spinnerEndTime = (Spinner) findViewById(R.id.spinnerEndTime);
        spinnerEndTime.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.times_array)));
        spinnerWeekDay2 = (Spinner) findViewById(R.id.spinnerWeekDay2);
        spinnerWeekDay2.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.weekday_array)));
        spinnerStartTime2 = (Spinner) findViewById(R.id.spinnerStartTime2);
        spinnerStartTime2.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.times_array)));
        spinnerEndTime2 = (Spinner) findViewById(R.id.spinnerEndTime2);
        spinnerEndTime2.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.times_array)));

        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new RegisterRequest().execute();
            }
        });
    }

    private class RegisterRequest extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                URL url = new URL("http://ulifefb.herokuapp.com/api/courses.json?access_token=" + MainActivity.getToken(CreateCourseActivity.this));
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.connect();

                JSONObject time1 = new JSONObject();
                time1.put("start_time", spinnerStartTime.getSelectedItem().toString());
                time1.put("end_time", spinnerEndTime.getSelectedItem().toString());
                time1.put("weekday", spinnerWeekDay.getSelectedItem().toString());
                JSONObject time2 = new JSONObject();
                time2.put("start_time", spinnerStartTime2.getSelectedItem().toString());
                time2.put("end_time", spinnerEndTime2.getSelectedItem().toString());
                time2.put("weekday", spinnerWeekDay2.getSelectedItem().toString());

                JSONObject jsonArray = new JSONObject();
                jsonArray.put("0", time1);
                jsonArray.put("1", time2);

                JSONObject jsonMaster = new JSONObject();
                JSONObject json = new JSONObject();
                json.put("name", editCourseName.getText().toString());
                json.put("class_time", jsonArray);
                jsonMaster.put("course", json);

                OutputStream os = httpURLConnection.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
                osw.write(jsonMaster.toString());
                osw.flush();
                osw.close();

                int responseCode = httpURLConnection.getResponseCode();
                if (responseCode == 200) {
                    Log.i("SINGUP", "OK");
                } else {
                    Log.i("SINGUP", "FAIL");
                    return false;
                }
                Scanner scanner = new Scanner(httpURLConnection.getInputStream(), "UTF-8");
                String result = "";
                do {
                    result += scanner.nextLine();
                } while (scanner.hasNextLine());

                Log.i("SINGUP", "RESTULT: " + result);
                JSONObject jsonResult = new JSONObject(result.replaceAll("&quot;", "\""));

            } catch (MalformedURLException e) {
                return false;
            } catch (IOException e) {
                return false;
            } catch (JSONException e) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean ok) {
            if (ok) {
                Toast.makeText(CreateCourseActivity.this, "Course successfully created", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(CreateCourseActivity.this, "Failed while creating course", Toast.LENGTH_SHORT).show();
            }
        }
    }
}