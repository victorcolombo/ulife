/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.view;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TabHost;
import com.hackathon.ulife.R;

/**
 * Created by victorcolombo on 5/16/15.
 */
public class CourseActivity extends Activity {

    private TabHost tabHost;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.course);
    }
}