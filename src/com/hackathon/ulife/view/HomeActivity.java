/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.view;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.hackathon.ulife.ClassTimeAdapter;
import com.hackathon.ulife.R;
import com.hackathon.ulife.model.ClassTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created by victorcolombo on 5/16/15.
 */
public class HomeActivity extends Activity {

    private Button buttonViewCourses;
    private Button buttonPrev;
    private Button buttonNext;
    private TextView textDay;
    private int idUser;
    private ListView listViewSchedule;
    private ClassTimeAdapter[] adapters = new ClassTimeAdapter[7];
    private ArrayList<ClassTime>[] lists = new ArrayList[7];
    private int today;
    private TextView textNoClasses;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        idUser = MainActivity.getToken(this);
        if (idUser == -1) finish();

        textDay = (TextView) findViewById(R.id.textDay);
        textNoClasses = (TextView) findViewById(R.id.textNoClasses);
        textNoClasses.setVisibility(View.VISIBLE);

        buttonViewCourses = (Button) findViewById(R.id.buttonViewCourses);
        buttonViewCourses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(HomeActivity.this, CoursesActivity.class);
                startActivity(it);
            }
        });

        buttonNext = (Button) findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                today++;
                today %= 7;
                listViewSchedule.setAdapter(adapters[today]);
                textDay.setText(getResources().getStringArray(R.array.weekday_array)[today]);
                if (lists[today].size() > 0) {
                    textNoClasses.setVisibility(View.INVISIBLE);
                } else {
                    textNoClasses.setVisibility(View.VISIBLE);
                }
            }
        });

        buttonPrev = (Button) findViewById(R.id.buttonPrev);
        buttonPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                today--;
                today += 7;
                today %= 7;
                listViewSchedule.setAdapter(adapters[today]);
                textDay.setText(getResources().getStringArray(R.array.weekday_array)[today]);
                if (lists[today].size() > 0) {
                    textNoClasses.setVisibility(View.INVISIBLE);
                } else {
                    textNoClasses.setVisibility(View.VISIBLE);
                }
            }
        });

        Calendar calendar = Calendar.getInstance();
        today = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        textDay.setText(getResources().getStringArray(R.array.weekday_array)[today]);
        for (int i = 0; i < 7; i++) {
            lists[i] = new ArrayList<ClassTime>();
            adapters[i] = new ClassTimeAdapter(this, lists[i]);
        }

        listViewSchedule = (ListView) findViewById(R.id.listViewSchedule);
        listViewSchedule.setAdapter(adapters[today]);
    }

    @Override
    public void onResume() {
        super.onResume();
        new CourseFetcherRequest().execute();
    }

    private class CourseFetcherRequest extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                URL url = new URL("http://ulifefb.herokuapp.com/api/courses.json?access_token=" + MainActivity.getToken(HomeActivity.this));
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();

                int responseCode = httpURLConnection.getResponseCode();
                if (responseCode == 200) {
                    Log.i("FETCHER", "OK");
                } else {
                    Log.i("FETCHER", "FAIL");
                    return false;
                }
                Scanner scanner = new Scanner(httpURLConnection.getInputStream(), "UTF-8");
                String result = "";
                do {
                    result += scanner.nextLine();
                } while (scanner.hasNextLine());
                JSONArray jsonResult = new JSONArray(result.replaceAll("&quot;", "\""));
                for (int i = 0; i < 7; i++) lists[i].clear();
                String[] helper = getResources().getStringArray(R.array.weekday_array);
                for (int i = 0; i < jsonResult.length(); i++) {
                    JSONObject course = jsonResult.getJSONObject(i);
                    JSONArray classtimes = course.getJSONArray("class_times");
                    for (int k = 0; k < classtimes.length(); k++) {
                        String weekday = classtimes.getJSONObject(k).getString("weekday");
                        int idxWeekday = 0;
                        for (int j = 0; j < 7; j++) {
                            Log.i("HOME", helper[j] + " - " + weekday);
                            if (helper[j].equals(weekday)) {
                                idxWeekday = j;
                                break;
                            }
                        }
                        lists[idxWeekday].add(new ClassTime(helper[idxWeekday], classtimes.getJSONObject(k).getString("start_time"),
                                classtimes.getJSONObject(k).getString("end_time"), course.getString("name")));
                    }
                }
                for (int i = 0; i < 7; i++) Collections.sort(lists[i]);
            } catch (MalformedURLException e) {
                return false;
            } catch (IOException e) {
                return false;
            } catch (JSONException e) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean ok) {
            if (ok) {
                adapters[today].notifyDataSetChanged();
                if (lists[today].size() > 0) {
                    textNoClasses.setVisibility(View.INVISIBLE);
                } else {
                    textNoClasses.setVisibility(View.VISIBLE);
                }
            } else {
                Log.i("FETCHER", "FAIL");
            }
        }
    }
}