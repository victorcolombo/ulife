/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.view;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.hackathon.ulife.R;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by victorcolombo on 5/16/15.
 */
public class SignUpActivity extends Activity {

    private Button buttonSignUp;
    private EditText editName, editPass, editPassConfirm, editEmail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        buttonSignUp = (Button) findViewById(R.id.buttonSignUp);
        editName = (EditText) findViewById(R.id.editName);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editPass = (EditText) findViewById(R.id.editPass);
        editPassConfirm = (EditText) findViewById(R.id.editConfirmPass);

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editPass.getText().toString().equals(editPassConfirm.getText().toString())) {
                    RegisterRequest registerRequest = new RegisterRequest();
                    registerRequest.execute();
                } else {
                    Toast.makeText(SignUpActivity.this, "The passwords do not match", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private class RegisterRequest extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                URL url = new URL("http://ulifefb.herokuapp.com/api/users/sign_up.json");
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.connect();

                JSONObject jsonMaster = new JSONObject();
                JSONObject json = new JSONObject();
                json.put("email", editEmail.getText().toString());
                json.put("password", editPass.getText().toString());
                json.put("name", editName.getText().toString());
                jsonMaster.put("user", json);

                OutputStream os = httpURLConnection.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
                osw.write(jsonMaster.toString());
                osw.flush();
                osw.close();

                int responseCode = httpURLConnection.getResponseCode();
                if (responseCode == 200) {
                    Log.i("SINGUP", "OK");
                } else {
                    Log.i("SINGUP", "FAIL");
                    return false;
                }
                Scanner scanner = new Scanner(httpURLConnection.getInputStream(), "UTF-8");
                String result = "";
                do {
                    result += scanner.nextLine();
                } while (scanner.hasNextLine());

                Log.i("SINGUP", "RESTULT: " + result);
                JSONObject jsonResult = new JSONObject(result.replaceAll("&quot;", "\""));
                int id = jsonResult.getInt("id");
                Log.i("SINGUP", "ID: " + id);
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("TOKEN", id);
                editor.apply();
                Log.i("SINGUP", "REGISTRED TOKEN");
            } catch (MalformedURLException e) {
                return false;
            } catch (IOException e) {
                return false;
            } catch (JSONException e) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean ok) {
            if (ok) {
                Toast.makeText(SignUpActivity.this, "Your account was created successfully!", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(SignUpActivity.this, "Failed while signing up", Toast.LENGTH_SHORT).show();
            }
        }
    }
}