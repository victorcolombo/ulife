package com.hackathon.ulife.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.hackathon.ulife.R;

public class MainActivity extends Activity {

    public static int getToken(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getInt("TOKEN", -1);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    @Override
    public void onResume() {
        super.onResume();

        int userId = getToken(this);
        if (userId == -1) {
            Intent it = new Intent(this, SignUpActivity.class);
            startActivity(it);
        } else {
            Intent it = new Intent(this, HomeActivity.class);
            startActivity(it);
        }
    }
}
