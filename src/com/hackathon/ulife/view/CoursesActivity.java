/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.view;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.hackathon.ulife.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by victorcolombo on 5/16/15.
 */
public class CoursesActivity extends Activity {

    private Button buttonCreateCourse;
    private ListView listViewCourses;
    private ArrayAdapter<String> adapterListView;
    private List<String> courses;
    private TextView textNoCourses;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.courses);

        textNoCourses = (TextView) findViewById(R.id.textNoCourses);
        textNoCourses.setVisibility(View.VISIBLE);
        courses = new ArrayList<String>();

        listViewCourses = (ListView) findViewById(R.id.listViewCourses);
        adapterListView = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, courses);
        listViewCourses.setAdapter(adapterListView);

        buttonCreateCourse = (Button) findViewById(R.id.buttonCreateCourse);
        buttonCreateCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(CoursesActivity.this, CreateCourseActivity.class);
                startActivity(it);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        new CourseFetcherRequest().execute();
    }

    private class CourseFetcherRequest extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                URL url = new URL("http://ulifefb.herokuapp.com/api/courses.json?access_token=" + MainActivity.getToken(CoursesActivity.this));
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();

                int responseCode = httpURLConnection.getResponseCode();
                if (responseCode == 200) {
                    Log.i("COURSES", "OK");
                } else {
                    Log.i("COURSES", "FAIL");
                    return false;
                }
                Scanner scanner = new Scanner(httpURLConnection.getInputStream(), "UTF-8");
                String result = "";
                do {
                    result += scanner.nextLine();
                } while (scanner.hasNextLine());
                JSONArray jsonResult = new JSONArray(result.replaceAll("&quot;", "\""));
                courses.clear();
                for (int i = 0; i < jsonResult.length(); i++) {
                    JSONObject course = jsonResult.getJSONObject(i);
                    courses.add(course.getString("name"));
                }
            } catch (MalformedURLException e) {
                return false;
            } catch (IOException e) {
                return false;
            } catch (JSONException e) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean ok) {
            if (ok) {
                adapterListView.notifyDataSetChanged();
                if (courses.size() > 0) {
                    textNoCourses.setVisibility(View.INVISIBLE);
                } else {
                    textNoCourses.setVisibility(View.VISIBLE);
                }
            } else {
                Log.i("COURSES", "FAIL");
            }
        }
    }
}