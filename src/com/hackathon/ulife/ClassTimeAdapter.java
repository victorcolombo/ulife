/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.hackathon.ulife.model.ClassTime;

import java.util.List;

public class ClassTimeAdapter extends ArrayAdapter<ClassTime> {

    private List<ClassTime> classTimes;

    public ClassTimeAdapter(Context context, List<ClassTime> classTimes) {
        super(context, android.R.layout.simple_list_item_2, android.R.id.text1, classTimes);
        this.classTimes = classTimes;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView tvNome = (TextView) view.findViewById(android.R.id.text1);
        TextView tvData = (TextView) view.findViewById(android.R.id.text2);
        tvNome.setText(classTimes.get(position).getName());
        tvData.setText(classTimes.get(position).getStartTime() + " - " + classTimes.get(position).getEndTime());
        return view;
    }
}
