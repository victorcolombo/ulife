/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.model;

import com.orm.SugarRecord;

public class Assignment extends SugarRecord<Assignment> {
    private String deadline;
    private String name;
    private Course course;

    public Assignment() {
    }

    public Assignment(String name, String deadline, Course course) {
        this.name = name;
        this.deadline = deadline;
        this.course = course;
    }
}
