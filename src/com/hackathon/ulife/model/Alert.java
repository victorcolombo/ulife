/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.model;

import com.orm.SugarRecord;

public class Alert extends SugarRecord<Alert> {
    private String name;
    private String message;
    private Course course;

    public Alert() {
    }

    public Alert(String name, String message, Course course) {
        this.name = name;
        this.message = message;
        this.course = course;
    }
}
