/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.model;

public class Task {
    private String name;
    private String text;
    private Group group;

    public Task() {
    }

    public Task(String name, String text, Group group) {
        this.name = name;
        this.text = text;
        this.group = group;
    }

}
