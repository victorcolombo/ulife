/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.model;

public class Test {

    private String date;
    private String startTime;
    private String endTime;
    private String name;
    private Course course;

    public Test() {
    }

    public Test(String name, String startTime, String endTime, String date, Course course) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.date = date;
        this.course = course;
    }
}
