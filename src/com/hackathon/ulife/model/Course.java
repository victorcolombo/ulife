/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.model;

import com.orm.SugarRecord;

public class Course extends SugarRecord<Course> {
    private String name;

    public Course() {
    }

    public Course(String name) {
        this.name = name;
    }
}
