/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.model;

public class Group {
    public static int CLOSED = 0;
    public static int OPEN = 1;

    private String name;
    private Assignment assignment;
    private String status;
    private int number;
    private int participationQuantity;

    public Group() {
    }

    public Group(String name, Assignment assignment, String status, int number, int participationQuantity) {
        this.name = name;
        this.assignment = assignment;
        this.status = status;
        this.participationQuantity = participationQuantity;
        this.number = number;
    }
}
