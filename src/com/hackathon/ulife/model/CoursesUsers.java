/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.model;

public class CoursesUsers {
    private Course course;
    private User user;

    public CoursesUsers() {
    }

    public CoursesUsers(Course course, User user) {
        this.course = course;
        this.user = user;
    }
}
