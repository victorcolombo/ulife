/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.model;

public class Message {
    private String text;
    private Participation participation;

    public Message() {
    }

    public Message(String text, Participation participation) {
        this.text = text;
        this.participation = participation;
    }
}
