/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.model;

import com.orm.SugarRecord;

public class ClassTime extends SugarRecord<ClassTime> implements Comparable<ClassTime> {

    private String name;
    private String weekDay;
    private String startTime;
    private String endTime;

    public ClassTime() {
    }

    public ClassTime(String weekDay, String startTime, String endTime, String name) {
        this.weekDay = weekDay;
        this.startTime = startTime;
        this.endTime = endTime;
        this.name = name;
    }

    public String getWeekDay() {
        return weekDay;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(ClassTime classTime) {
        int a = Integer.parseInt(getStartTime().split(":")[0]);
        int b = Integer.parseInt(classTime.getStartTime().split(":")[0]);
        int A = Integer.parseInt(getStartTime().split(":")[1]);
        int B = Integer.parseInt(classTime.getStartTime().split(":")[1]);
        if (a != b) return (a < b) ? -1 : 1;
        else if (a != b) return (A < B) ? -1 : 1;
        else return 0;
    }
}
