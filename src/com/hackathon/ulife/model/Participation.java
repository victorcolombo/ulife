/*
 * Copyright (c) 2015 Victor de Oliveira Colombo. Todos os direitos reservados.
 */

package com.hackathon.ulife.model;

public class Participation {
    private User user;
    private Group group;

    public Participation() {
    }

    public Participation(User user, Group group) {
        this.user = user;
        this.group = group;
    }
}
